# require objectstore
require_relative './object_store'
# class Play
class Play
  include ObjectStore
  attr_accessor :age, :fname, :email

  validate_presence_of :fname, :email

  def validate
    age == 23
  end
end

ob = Play.new
ob.fname = "abc"
ob.age = 23
ob.email = "abc@vinsol.com"
p ob
p ob.save
p ob.errors


person2 = Play.new
person2.fname = "ac"
person2.age = 23
person2.email = "abc2@vinsol.com"
p person2

p person2.save
p person2.errors


p Play.collect
p Play.count
p Play.methods.grep /find_by_/
p Play.find_by_fname("abc")