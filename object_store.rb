module ObjectStore
  module ClassMethods
    def validate_presence_of(*arguments)
      @validate_attributes = arguments
      initialize_class
      create_dynamic_finders
    end

    def initialize_class
      class_eval do
        @valid_objects = []
        attr_accessor :errors, :valid
        def initialize
          @valid = false
          @errors = Hash.new([])
        end
      end
    end

    def create_dynamic_finders
      @validate_attributes.each do |attr|
        define_singleton_method("find_by_#{attr}") do |arg|
          @valid_objects.select { |object| object.send(attr).eql?(arg) }
        end
      end
    end

    def save_object(object)
      object.valid = @validate_attributes.all? do |attribute|
        valid = object.instance_variable_defined?("@#{attribute}")
        if !valid
          error_in_attribute(object, attribute)
        end
        valid
      end
      push_to_valid_object(object) if object.valid
      object.valid
    end

    def push_to_valid_object(object)
      @valid_objects.push(object)
    end

    def error_in_attribute(object, attribute)
      object.errors[attribute] += ['Cant be blank']
    end

    def collect
      @valid_objects
    end

    def count
      @valid_objects.length
    end
  end

  module InstanceMethods
    def save
      if validate
        self.class.save_object(self)
      else
        "Object is not valid"
      end
    end
  end

  def self.included(receiver)
    receiver.extend(ClassMethods)
    receiver.send :include, InstanceMethods
  end
end
